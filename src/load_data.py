'''
 - This script loads data and creates a training and test set.
 - In this example we read data stored locally (although tracked), 
   in pracice you would download data from storage in this script.
 - User must specify path to data set as a command line argument.
'''

import os
import sys
import yaml
import pandas as pd

from typing import Dict, List, Tuple
from sklearn.model_selection import StratifiedShuffleSplit

    
def load_data_from_txt(path: str, columns: List[str]) -> pd.DataFrame:
    return pd.read_csv(path, names=columns)
    
def make_stratified_split(df: pd.DataFrame, 
                          n_splits: int,
                          test_ratio: float,
                          random_state: float,
                          target: str) -> Tuple[pd.DataFrame,pd.DataFrame]:
                          
    X, y = df[[col for col in list(df) if col != target]], df[target]
    sss = StratifiedShuffleSplit(n_splits=n_splits,test_size=test_ratio,random_state=random_state)                 
    for train_index, test_index in sss.split(X,y):
        train_data = df.loc[train_index]
        test_data = df.loc[test_index]
    return train_data, test_data
 
 
#check the correct number of arguments have been used with python command (the path to the local data)
if len(sys.argv) != 2:
    sys.stderr.write("Arguments error. Usage:\n")
    sys.stderr.write("\tpython load_data.py data-file\n")
    sys.exit(1)
    
#read params from params.yml
params: Dict[str,float] = yaml.safe_load(open('params.yaml'))['prepare']
n_splits: int = params['n_splits']
test_ratio: float = params['test_ratio']
random_state: int = params['seed']
target: str = params['target']

#parse input
input = sys.argv[1]

#generate data
columns: List[str] = ['sepal length','sepal width in cm', 'petal length','petal width','class']
data = load_data_from_txt(input,columns)
train_data, test_data = make_stratified_split(data, n_splits, test_ratio, random_state, target)

#save data
output_directory: str = os.path.join(os.path.dirname('__file__'),'data','processed_data')
if not os.path.exists(output_directory):
    os.makedirs(output_directory)
train_data.to_csv(os.path.join(output_directory,'train_data.csv'))
test_data.to_csv(os.path.join(output_directory,'test_data.csv'))